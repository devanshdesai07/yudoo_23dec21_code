package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.R
import com.yudoo.databinding.OrderItemLayoutBinding
import com.yudoo.databinding.OrderItemsItemLayoutBinding
import com.yudoo.model.ModelOrderItem
import com.yudoo.utils.SpannableUtils
import java.util.*

class OrderItemsAdapter(listener: OnItemClickListener<ModelOrderItem>? = null) :
    BaseFilterAdapter<ModelOrderItem, OrderItemsAdapter.OrderViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OrderViewHolder {
        val itemBinding =
            OrderItemsItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OrderViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelOrderItem>,
        item: ModelOrderItem,
        searchText: String
    ) {
    }

    inner class OrderViewHolder(private var itemBinding: OrderItemsItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(order: ModelOrderItem?) {
            itemBinding.tvName.text = order?.itemName
            itemBinding.tvAmount.text = order?.amount

        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}