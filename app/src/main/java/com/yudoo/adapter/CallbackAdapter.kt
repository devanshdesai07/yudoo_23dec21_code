package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.R
import com.yudoo.databinding.CallbackItemLayoutBinding
import com.yudoo.model.ModelCallback
import java.util.*

class CallbackAdapter(listener: OnItemClickListener<ModelCallback>? = null) :
    BaseFilterAdapter<ModelCallback, CallbackAdapter.CallbackViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CallbackViewHolder {
        val itemBinding =
            CallbackItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CallbackViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CallbackViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelCallback>,
        item: ModelCallback,
        searchText: String
    ) {
    }

    inner class CallbackViewHolder(private var itemBinding: CallbackItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(callback: ModelCallback?) {
            itemBinding.tvName.text = callback?.userName
            itemBinding.tvOnlineOffline.text = callback?.onlineStatus
            itemBinding.tvPastOrder.text = callback?.pastOrder
            itemBinding.tvLastOrderDate.text = callback?.lastOrderDate
            itemBinding.tvLastCallDate.text = callback?.lastCallDate

            itemBinding.tvOnlineOffline.setTextColor(
                ContextCompat.getColor(
                    itemBinding.tvOnlineOffline.context,
                    if (callback?.onlineStatus?.lowercase() == "online") R.color.colorGreen
                    else R.color.colorRed
                )
            )
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}