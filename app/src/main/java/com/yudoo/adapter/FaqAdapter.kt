package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.R
import com.yudoo.customviews.expandable.State
import com.yudoo.databinding.FaqItemLayoutBinding
import com.yudoo.model.ModelFaq
import java.util.*

class FaqAdapter(listener: OnItemClickListener<ModelFaq>? = null) :
    BaseFilterAdapter<ModelFaq, FaqAdapter.FaqViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FaqViewHolder {
        val itemBinding =
            FaqItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FaqViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: FaqViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelFaq>,
        item: ModelFaq,
        searchText: String
    ) {
    }

    inner class FaqViewHolder(private var itemBinding: FaqItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(faq: ModelFaq?) {
            itemBinding.tvQuestion.text = faq?.question
            itemBinding.tvDescription.text = faq?.description
            itemBinding.tvQuestion.setOnClickListener(this)

            itemBinding.elFaqDescription.setOnExpansionUpdateListener { _, state ->
                itemBinding.ivExpandCollapse.setImageResource(
                    if (state == State.EXPANDED || state == State.EXPANDING) R.drawable.ic_round_remove
                    else R.drawable.ic_round_add
                )
            }
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                itemBinding.elFaqDescription.toggle()
            }
        }

    }
}