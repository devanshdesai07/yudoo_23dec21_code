package com.yudoo.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.databinding.HomeScreenCardItemBinding
import com.yudoo.model.ModelHomeCard
import com.yudoo.utils.AppUtils
import java.util.*

class HomeCardAdapter(listener: OnItemClickListener<ModelHomeCard>? = null) :
    BaseFilterAdapter<ModelHomeCard, HomeCardAdapter.HomeCardViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HomeCardViewHolder {
        val itemBinding =
            HomeScreenCardItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HomeCardViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: HomeCardViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelHomeCard>,
        item: ModelHomeCard,
        searchText: String
    ) {
    }

    inner class HomeCardViewHolder(private var itemBinding: HomeScreenCardItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(homeCard: ModelHomeCard?) {
            itemBinding.tvCount.text = homeCard?.count
            itemBinding.tvDescription.text = homeCard?.title

            if ((homeCard?.icon ?: 0) != 0) {
                itemBinding.fabIcon.setImageResource(homeCard?.icon ?: 0)
            }
            if (homeCard?.colorBg != null) {
                /*itemBinding.flIconBg.setBackgroundColor(
                    AppUtils.getColorFromString(
                        homeCard.colorBg
                    )
                )*/
                itemBinding.flIconBg.backgroundTintList = ColorStateList.valueOf(
                    AppUtils.getColorFromString(
                        homeCard.colorBg
                    )
                )
            }
            if (homeCard?.color != null) {
                itemBinding.fabIcon.imageTintList = ColorStateList.valueOf(
                    AppUtils.getColorFromString(
                        homeCard.color
                    )
                )
            }
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}