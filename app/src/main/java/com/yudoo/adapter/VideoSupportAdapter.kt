package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.databinding.VideoSupportItemLayoutBinding
import com.yudoo.model.ModelVideo
import java.util.*

class VideoSupportAdapter(listener: OnItemClickListener<ModelVideo>? = null) :
    BaseFilterAdapter<ModelVideo, VideoSupportAdapter.VideoSupportViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VideoSupportViewHolder {
        val itemBinding =
            VideoSupportItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return VideoSupportViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: VideoSupportViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelVideo>,
        item: ModelVideo,
        searchText: String
    ) {
    }

    inner class VideoSupportViewHolder(private var itemBinding: VideoSupportItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(video: ModelVideo?) {
            itemBinding.tvTitle.text = video?.title
            itemBinding.tvDescription.text = video?.description
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}