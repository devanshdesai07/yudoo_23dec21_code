package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.R
import com.yudoo.databinding.OrderItemLayoutBinding
import com.yudoo.model.ModelOrder
import com.yudoo.utils.SpannableUtils
import java.util.*

class OrderAdapter(listener: OnItemClickListener<ModelOrder>? = null) :
    BaseFilterAdapter<ModelOrder, OrderAdapter.OrderViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OrderViewHolder {
        val itemBinding =
            OrderItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OrderViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelOrder>,
        item: ModelOrder,
        searchText: String
    ) {
    }

    inner class OrderViewHolder(private var itemBinding: OrderItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(order: ModelOrder?) {
            itemBinding.tvItemName.text = order?.itemName
            itemBinding.tvItemId.text = order?.itemId
            itemBinding.tvShippingRequest.text =
                String.format(
                    "%s %s",
                    itemBinding.tvShippingRequest.context.getString(R.string.shipping_request),
                    order?.shippingStatus
                )

            SpannableUtils.newInstance().setColorSpan(
                itemBinding.tvShippingRequest, order?.shippingStatus, ContextCompat.getColor(
                    itemBinding.tvShippingRequest.context,
                    if (order?.shippingStatus?.lowercase() == "pending") R.color.colorRed
                    else R.color.colorGreen
                )
            )
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}