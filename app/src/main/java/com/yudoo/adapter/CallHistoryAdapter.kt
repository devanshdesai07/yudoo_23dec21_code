package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.databinding.CallhistoryItemLayoutBinding
import com.yudoo.model.ModelCallHistory
import java.util.*

class CallHistoryAdapter(listener: OnItemClickListener<ModelCallHistory>? = null) :
    BaseFilterAdapter<ModelCallHistory, CallHistoryAdapter.CallHistoryViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CallHistoryViewHolder {
        val itemBinding =
            CallhistoryItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CallHistoryViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CallHistoryViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelCallHistory>,
        item: ModelCallHistory,
        searchText: String
    ) {
    }

    inner class CallHistoryViewHolder(private var itemBinding: CallhistoryItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(callHistory: ModelCallHistory?) {
            itemBinding.tvName.text = callHistory?.userName
            itemBinding.tvDuration.text = callHistory?.callDuration
            itemBinding.tvTime.text = callHistory?.callTime
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}