package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.databinding.RatingItemLayoutBinding
import com.yudoo.model.ModelRating
import java.util.*

class RatingAdapter(listener: OnItemClickListener<ModelRating>? = null) :
    BaseFilterAdapter<ModelRating, RatingAdapter.RatingViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RatingViewHolder {
        val itemBinding =
            RatingItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RatingViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RatingViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelRating>,
        item: ModelRating,
        searchText: String
    ) {
    }

    inner class RatingViewHolder(private var itemBinding: RatingItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(rating: ModelRating?) {
            itemBinding.tvOverAllRatting.text = String.format("(%s)", rating?.overallRating)
            itemBinding.tvLast20Ratting.text = String.format("(%s)", rating?.last20)
            itemBinding.tvRatingTitle.text = rating?.title

            if (rating?.titleIcon != 0) {
                itemBinding.tvRatingTitle.setCompoundDrawablesWithIntrinsicBounds(
                    rating?.titleIcon ?: 0, 0, 0, 0
                )
            }
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}