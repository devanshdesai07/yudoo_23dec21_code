package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.databinding.ShopInfoTimingItemLayoutBinding
import com.yudoo.model.ModelTimeSelection
import java.util.*

class TimeSelectionAdapter(listener: OnItemClickListener<ModelTimeSelection>? = null) :
    BaseFilterAdapter<ModelTimeSelection, TimeSelectionAdapter.TimeSelectionViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TimeSelectionViewHolder {
        val itemBinding =
            ShopInfoTimingItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return TimeSelectionViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: TimeSelectionViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelTimeSelection>,
        item: ModelTimeSelection,
        searchText: String
    ) {
    }

    inner class TimeSelectionViewHolder(private var itemBinding: ShopInfoTimingItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(timeSelection: ModelTimeSelection?) {
            itemBinding.chkDayName.text = timeSelection?.dayName
            itemBinding.chkDayName.isChecked = timeSelection?.isChecked==true
            itemBinding.startTime.tvTime.text = timeSelection?.startTime
            itemBinding.endTime.tvTime.text = timeSelection?.endTime

            itemBinding.startTime.tvTime.setOnClickListener(this)
            itemBinding.endTime.tvTime.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}