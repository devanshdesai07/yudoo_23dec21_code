package com.yudoo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.databinding.ReviewItemLayoutBinding
import com.yudoo.model.ModelReview
import java.util.*

class ReviewAdapter(listener: OnItemClickListener<ModelReview>? = null) :
    BaseFilterAdapter<ModelReview, ReviewAdapter.ReviewViewHolder>() {

    init {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ReviewViewHolder {
        val itemBinding =
            ReviewItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReviewViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.onBind(getListItem(position))
    }

    override fun filterObject(
        filteredList: ArrayList<ModelReview>,
        item: ModelReview,
        searchText: String
    ) {
    }

    inner class ReviewViewHolder(private var itemBinding: ReviewItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        fun onBind(review: ModelReview?) {
            itemBinding.tvReviewMessage.text = review?.description
            itemBinding.tvName.text = String.format("By %s", review?.userName ?: "")
            itemBinding.tvDate.text = review?.date
        }

        override fun onClick(v: View?) {
            if (adapterPosition >= 0) {
                onItemClickListener?.onItemClick(v, getListItem(adapterPosition), adapterPosition)
            }
        }

    }
}