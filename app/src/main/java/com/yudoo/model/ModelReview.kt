package com.yudoo.model

class ModelReview() {
    var description: String? = null
    var date: String? = null
    var userName: String? = null

    constructor(
        description: String?,
        date: String?,
        userName: String?
    ) : this() {
        this.description = description
        this.date = date
        this.userName = userName
    }
}