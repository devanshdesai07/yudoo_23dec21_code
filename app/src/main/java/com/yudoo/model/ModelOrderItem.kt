package com.yudoo.model

class ModelOrderItem() {
    var itemName: String? = null
    var quantity: String? = null
    var size: String? = null
    var amount: String? = null

    constructor(
        itemName: String?,
        quantity: String?,
        size: String?,
        amount: String?
    ) : this() {
        this.itemName = itemName
        this.quantity = quantity
        this.size = size
        this.amount = amount
    }
}