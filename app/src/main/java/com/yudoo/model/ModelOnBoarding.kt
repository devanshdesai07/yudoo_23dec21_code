package com.yudoo.model

import android.os.Parcel
import android.os.Parcelable

class ModelOnBoarding() : Parcelable {
    var image: Int = 0
    var eclipseImage: Int = 0
    var eclipseImageGravity: Int = 0
    var description: String? = null

    constructor(parcel: Parcel) : this() {
        image = parcel.readInt()
        eclipseImage = parcel.readInt()
        eclipseImageGravity = parcel.readInt()
        description = parcel.readString()
    }

    constructor(
        image: Int = 0,
        eclipseImage: Int = 0,
        eclipseImageGravity: Int = 0,
        description: String? = null
    ) : this() {
        this.image = image
        this.eclipseImage = eclipseImage
        this.eclipseImageGravity = eclipseImageGravity
        this.description = description
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(image)
        parcel.writeInt(eclipseImage)
        parcel.writeInt(eclipseImageGravity)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelOnBoarding> {
        override fun createFromParcel(parcel: Parcel): ModelOnBoarding {
            return ModelOnBoarding(parcel)
        }

        override fun newArray(size: Int): Array<ModelOnBoarding?> {
            return arrayOfNulls(size)
        }
    }

}