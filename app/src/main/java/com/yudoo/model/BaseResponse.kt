package com.yudoo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


open class BaseResponse {
    @SerializedName("Status")
    @Expose
    var status: String? = null

    @SerializedName("Errors")
    @Expose
    var errors: String? = null
}