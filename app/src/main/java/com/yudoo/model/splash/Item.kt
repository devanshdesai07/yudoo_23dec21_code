package com.yudoo.model.splash


import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("Message")
    val message: String,
    @SerializedName("VersionId")
    val versionId: Int,
    @SerializedName("VersionName")
    val versionName: String
)