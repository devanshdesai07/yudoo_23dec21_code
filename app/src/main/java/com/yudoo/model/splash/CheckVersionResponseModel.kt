package com.yudoo.model.splash


import com.google.gson.annotations.SerializedName

data class CheckVersionResponseModel(
    @SerializedName("Code")
    val code: Int,
    @SerializedName("IsSuccessStatusCode")
    val isSuccessStatusCode: Boolean,
    @SerializedName("IsValid")
    val isValid: Boolean,
    @SerializedName("Item")
    val item: Item,
    @SerializedName("Message")
    val message: Any
)