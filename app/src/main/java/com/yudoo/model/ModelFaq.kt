package com.yudoo.model

class ModelFaq() {
    var question: String? = null
    var description: String? = null

    constructor(
        question: String?,
        description: String?
    ) : this() {
        this.question = question
        this.description = description
    }
}