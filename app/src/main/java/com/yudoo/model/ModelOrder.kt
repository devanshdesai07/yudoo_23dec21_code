package com.yudoo.model

class ModelOrder() {
    var itemName: String? = null
    var itemId: String? = null
    var shippingStatus: String? = null

    constructor(
        itemName: String?,
        itemId: String?,
        shippingStatus: String?
    ) : this() {
        this.itemName = itemName
        this.itemId = itemId
        this.shippingStatus = shippingStatus
    }
}