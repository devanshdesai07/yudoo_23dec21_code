package com.yudoo.model

class ModelHomeCard() {
    var count: String? = null
    var title: String? = null
    var icon: Int? = 0
    var color: String? = null
    var colorBg: String? = null

    constructor(
        count: String?,
        title: String?,
        icon: Int? = 0,
        color: String? = null,
        colorBg: String? = null
    ) : this() {
        this.count = count
        this.title = title
        this.icon = icon
        this.color = color
        this.colorBg = colorBg
    }
}