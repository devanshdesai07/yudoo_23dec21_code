package com.yudoo.model

import androidx.fragment.app.Fragment

data class ModelFragment(val fragment: Fragment, var title: String? = null)