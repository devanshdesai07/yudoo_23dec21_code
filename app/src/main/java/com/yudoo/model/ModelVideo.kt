package com.yudoo.model

class ModelVideo() {
    var video: String? = null
    var title: String? = null
    var description: String? = null

    constructor(
        video: String?,
        title: String?,
        description: String?,
    ) : this() {
        this.video = video
        this.title = title
        this.description = description
    }
}