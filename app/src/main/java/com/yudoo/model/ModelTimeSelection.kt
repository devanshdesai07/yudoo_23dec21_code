package com.yudoo.model

class ModelTimeSelection() {
    var dayName: String? = null
    var startTime: String? = null
    var endTime: String? = null
    var isChecked: Boolean = false

    constructor(
        dayName: String?,
        startTime: String?,
        endTime: String?,
        isChecked: Boolean = false
    ) : this() {
        this.dayName = dayName
        this.startTime = startTime
        this.endTime = endTime
        this.isChecked = isChecked
    }
}