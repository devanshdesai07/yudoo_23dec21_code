package com.yudoo.model.registration

class ShopOwnerData {
    var Id: String? = null
    var ShopName: String? = null
    var ShopOwnerName: String? = null
    var CategoryId: String? = null
    var AddressLine1: String? = null
    var AddressLine2: String? = null
    var LandMark: String? = null
    var PinCode: String? = null
    var VideoCall: String? = null
    var RegisteredDate: String? = null
    var ShopStatus: String? = null
    var Ratings: String? = null
    var RatingsBy: String? = null
    var RatingsCreatedDate: String? = null
    var RatingsUpdatedDate: String? = null
    var DeletedDate: String? = null
    var DeletedBy: String? = null
    var IsDeleted: String? = null
    var IsActive: String? = null
    var GstNumber: String? = null
    var MobileNumber: String? = null
    var PersonalPanNumber: String? = null
    var CompanyPanNumber: String? = null
    var CompanyRegisterAddress: String? = null
    var IsRegister: Boolean = false
}