package com.yudoo.model.registration

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.yudoo.model.BaseResponse

class ShopOwnerResponse : BaseResponse() {
    @SerializedName("Code")
    @Expose
    var Code: String? = null

    @SerializedName("Message")
    @Expose
    var Message: String? = null

    @SerializedName("Item")
    @Expose
    var Item: ShopOwnerData? = null
}