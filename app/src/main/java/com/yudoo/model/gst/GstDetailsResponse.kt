package com.yudoo.model.gst

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GstDetailsResponse {
    @SerializedName("taxpayerInfo")
    @Expose
    var taxpayerInfo: TaxpayerInfo? = null
}