package com.yudoo.model.gst

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class Addr {
    @SerializedName("bnm")
    @Expose
    var bnm: String? = null

    @SerializedName("st")
    @Expose
    var st: String? = null

    @SerializedName("loc")
    @Expose
    var loc: String? = null

    @SerializedName("bno")
    @Expose
    var bno: String? = null

    @SerializedName("dst")
    @Expose
    var dst: String? = null

    @SerializedName("stcd")
    @Expose
    var stcd: String? = null

    @SerializedName("city")
    @Expose
    var city: String? = null

    @SerializedName("flno")
    @Expose
    var flno: String? = null

    @SerializedName("lt")
    @Expose
    var lt: String? = null

    @SerializedName("pncd")
    @Expose
    var pncd: String? = null

    @SerializedName("lg")
    @Expose
    var lg: String? = null

    fun getFormattedAddress(): String {
        val address = ArrayList<String?>()
        if (!flno.isNullOrEmpty()) {
            address.add(flno)
        }
        if (!bno.isNullOrEmpty()) {
            address.add(bno)
        }
        if (!st.isNullOrEmpty()) {
            address.add(st)
        }
        if (!loc.isNullOrEmpty()) {
            address.add(loc)
        }
        if (!dst.isNullOrEmpty()) {
            address.add(dst)
        }
        if (!stcd.isNullOrEmpty()) {
            address.add(stcd)
        }
        if (!pncd.isNullOrEmpty()) {
            address.add(pncd)
        }
        return address.joinToString(", ")
    }
}