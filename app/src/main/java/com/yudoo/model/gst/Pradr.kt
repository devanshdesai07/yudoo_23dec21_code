package com.yudoo.model.gst

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class Pradr {
    @SerializedName("addr")
    @Expose
    var addr: Addr? = null

    @SerializedName("ntr")
    @Expose
    var ntr: String? = null
}