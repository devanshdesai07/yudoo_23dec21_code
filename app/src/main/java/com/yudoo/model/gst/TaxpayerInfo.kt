package com.yudoo.model.gst

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.yudoo.model.gst.Pradr

class TaxpayerInfo {
    @SerializedName("stjCd")
    @Expose
    var stjCd: String? = null

    @SerializedName("lgnm")
    @Expose
    var lgnm: String? = null

    @SerializedName("stj")
    @Expose
    var stj: String? = null

    @SerializedName("dty")
    @Expose
    var dty: String? = null

    @SerializedName("gstin")
    @Expose
    var gstin: String? = null

    @SerializedName("lstupdt")
    @Expose
    var lstupdt: String? = null

    @SerializedName("rgdt")
    @Expose
    var rgdt: String? = null

    @SerializedName("ctb")
    @Expose
    var ctb: String? = null

    @SerializedName("pradr")
    @Expose
    var pradr: Pradr? = null

    @SerializedName("tradeNam")
    @Expose
    var tradeNam: String? = null

    @SerializedName("sts")
    @Expose
    var sts: String? = null

    @SerializedName("ctjCd")
    @Expose
    var ctjCd: String? = null

    @SerializedName("ctj")
    @Expose
    var ctj: String? = null

    @SerializedName("panNo")
    @Expose
    var panNo: String? = null
}