package com.yudoo.model

class ModelRating() {
    var title: String? = null
    var titleIcon: Int = 0
    var overallRating: String? = null
    var last20: String? = null

    constructor(
        title: String?,
        titleIcon: Int,
        overallRating: String?,
        last20: String?
    ) : this() {
        this.title = title
        this.titleIcon = titleIcon
        this.overallRating = overallRating
        this.last20 = last20
    }
}