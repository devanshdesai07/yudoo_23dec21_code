package com.yudoo.model

class ModelCallHistory() {
    var userName: String? = null
    var callDuration: String? = null
    var callTime: String? = null

    constructor(
        userName: String?,
        callDuration: String?,
        callTime: String?
    ) : this() {
        this.userName = userName
        this.callDuration = callDuration
        this.callTime = callTime
    }
}