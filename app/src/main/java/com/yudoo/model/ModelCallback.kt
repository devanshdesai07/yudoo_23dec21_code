package com.yudoo.model

class ModelCallback() {
    var userName: String? = null
    var userImage: String? = null
    var onlineStatus: String? = null
    var pastOrder: String? = null
    var lastOrderDate: String? = null
    var lastCallDate: String? = null

    constructor(
        userName: String?,
        userImage: String?,
        onlineStatus: String?,
        pastOrder: String?,
        lastOrderDate: String?,
        lastCallDate: String?
    ) : this() {
        this.userName = userName
        this.userImage = userImage
        this.onlineStatus = onlineStatus
        this.pastOrder = pastOrder
        this.lastOrderDate = lastOrderDate
        this.lastCallDate = lastCallDate
    }
}