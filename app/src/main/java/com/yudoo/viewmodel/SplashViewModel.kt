package com.yudoo.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(application: Application) : ViewModel() {

    companion object {
        const val START_DELAY = 2000L
    }

    val splashStatus = MutableLiveData<Boolean>()

    fun initSplashScreen() {
        splashStatus.value = false
        viewModelScope.launch {
            delay(START_DELAY)
            splashStatus.value = true
        }
    }

}

class SplashViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SplashViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}