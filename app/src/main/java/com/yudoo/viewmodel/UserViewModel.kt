package com.yudoo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yudoo.api.RemoteCallback
import com.yudoo.model.gst.GstDetailsResponse
import com.yudoo.model.registration.ShopOwnerData
import com.yudoo.model.registration.ShopOwnerResponse
import com.yudoo.repository.UserRepository


class UserViewModel(private val repository: UserRepository) : ViewModel() {

    val sellerExistData = MutableLiveData<ShopOwnerResponse?>()
    val sellerNotExistData = MutableLiveData<ShopOwnerData?>()
    val loginData = MutableLiveData<ShopOwnerResponse?>()
    val gstDetailsData = MutableLiveData<GstDetailsResponse>()
    val otpData = MutableLiveData<String?>()

    val errorMessage = MutableLiveData<String>()
    val loadingStatus = MutableLiveData<Boolean>()
    val gstLoadingStatus = MutableLiveData<Boolean>()

    fun getGstNoDetails(gstNumber: String) {
        gstLoadingStatus.postValue(true)
        repository.getGstNoDetails(gstNumber)
            .enqueue(object : RemoteCallback<GstDetailsResponse>() {
                override fun onSuccess(response: GstDetailsResponse?) {
                    response?.let { gstDetailsData.postValue(it) }
                }

                override fun onFailed(throwable: Throwable) {
                    errorMessage.postValue(throwable.message)
                }

                override fun onInternetFailed() {
                    errorMessage.postValue("Internet Connection Failed")
                }

                override fun onComplete() {
                    gstLoadingStatus.postValue(false)
                }
            })
    }

    fun sendOtpOnMobile(mobileNumber: String) {
        loadingStatus.postValue(true)
        repository.sendOtpOnMobile(mobileNumber)
            .enqueue(object : RemoteCallback<String>() {
                override fun onSuccess(response: String?) {
                    otpData.postValue(response)
                }

                override fun onFailed(throwable: Throwable) {
                    errorMessage.postValue(throwable.message)
                }

                override fun onInternetFailed() {
                    errorMessage.postValue("Internet Connection Failed")
                }

                override fun onComplete() {
                    loadingStatus.postValue(false)
                }
            })
    }

    fun checkSellerExistByMobileAndId(
        shopId: String,
        mobile: String
    ) {
        loadingStatus.postValue(true)
        repository.checkSellerExistByMobileAndId(
            shopId,
            mobile
        ).enqueue(object : RemoteCallback<ShopOwnerResponse>() {
            override fun onSuccess(response: ShopOwnerResponse?) {
                loginData.postValue(response)
            }

            override fun onFailed(throwable: Throwable) {
                errorMessage.postValue(throwable.message)
            }

            override fun onInternetFailed() {
                errorMessage.postValue("Internet Connection Failed")
            }

            override fun onComplete() {
                loadingStatus.postValue(false)
            }
        })
    }


    fun checkSellerExist(request: ShopOwnerData) {
        loadingStatus.postValue(true)
        repository.checkSellerExist(request)
            .enqueue(object : RemoteCallback<ShopOwnerResponse>() {
                override fun onSuccess(response: ShopOwnerResponse?) {
                    sellerExistData.postValue(response)
                }

                override fun onFailed(throwable: Throwable) {
                    //errorMessage.postValue(throwable.message)
                    sellerNotExistData.postValue(request)
                }

                override fun onInternetFailed() {
                    errorMessage.postValue("Internet Connection Failed")
                }

                override fun onComplete() {
                    loadingStatus.postValue(false)
                }
            })
    }

    fun registerOrUpdateShopOwner(request: ShopOwnerData) {
        loadingStatus.postValue(true)
        repository.registerOrUpdateShopOwner(request)
            .enqueue(object : RemoteCallback<ShopOwnerResponse>() {
                override fun onSuccess(response: ShopOwnerResponse?) {
                    sellerExistData.postValue(response)
                }

                override fun onFailed(throwable: Throwable) {
                    errorMessage.postValue(throwable.message)
                }

                override fun onInternetFailed() {
                    errorMessage.postValue("Internet Connection Failed")
                }

                override fun onComplete() {
                    loadingStatus.postValue(false)
                }
            })
    }

}

class UserViewModelFactory(private val repository: UserRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}