package com.yudoo.repository

import com.yudoo.api.WebAPIService
import com.yudoo.model.gst.GstDetailsResponse
import com.yudoo.model.registration.ShopOwnerData
import com.yudoo.model.registration.ShopOwnerResponse
import retrofit2.Call

class UserRepository(private val apiServices: WebAPIService) {

    fun registerOrUpdateShopOwner(request: ShopOwnerData): Call<ShopOwnerResponse> {
        return apiServices.registerOrUpdateShopOwner(request)
    }

    fun checkSellerExist(request: ShopOwnerData): Call<ShopOwnerResponse> {
        return apiServices.checkSellerExist(
            request.ShopOwnerName,
            request.ShopName,
            request.MobileNumber
        );
    }

    fun checkSellerExistByMobileAndId(
        shopId: String,
        mobile: String
    ): Call<ShopOwnerResponse> {
        return apiServices.checkSellerExistByMobileAndId(shopId, mobile)
    }

    fun getGstNoDetails(
        gstNumber: String
    ): Call<GstDetailsResponse> {
        return apiServices.getGstNoDetails(gstNumber)
    }

    fun sendOtpOnMobile(mobileNumber: String): Call<String> {
        return apiServices.sendOtpOnMobile(mobileNumber)
    }

}