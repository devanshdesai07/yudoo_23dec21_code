package com.yudoo.listener

import android.os.Handler
import android.text.Editable
import android.text.TextWatcher

abstract class DelayTextWatcher(var delay: Long = 300) : TextWatcher {
    //private val DELAY: Long = 300 // milliseconds
    private val handler = Handler()
    private val runnable = Runnable { onAfterTextChanged() }

    override fun beforeTextChanged(
        s: CharSequence,
        start: Int,
        count: Int,
        after: Int
    ) {
    }

    override fun onTextChanged(
        s: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {
    }

    override fun afterTextChanged(s: Editable) {
        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, delay)
    }

    protected abstract fun onAfterTextChanged()
}