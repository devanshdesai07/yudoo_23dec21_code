package com.yudoo.extensions

import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.yudoo.R
import com.yudoo.utils.NetworkUtils

fun AppCompatActivity.setToolbarWithBack(toolbar: Toolbar, resId: Int) {
    setToolbarWithBack(toolbar, getString(resId))
}

fun AppCompatActivity.setToolbarWithBack(toolbar: Toolbar, title: String?) {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.title = title
}

fun AppCompatActivity.setToolbar(toolbar: Toolbar, resId: Int) {
    setToolbar(toolbar, getString(resId))
}

fun AppCompatActivity.setToolbar(toolbar: Toolbar, title: String?) {
    setSupportActionBar(toolbar)
    supportActionBar?.title = ""
    toolbar.findViewById<TextView>(R.id.toolbarTitle).text = title
}

fun AppCompatActivity.showToast(message: String?) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


fun AppCompatActivity.isInternetNotAvailable(): Boolean {
    return !NetworkUtils.isInternetAvailable(this)
}

fun AppCompatActivity.replaceFragments(
    fragment: Fragment?,
    addToBackStack: Boolean,
    tag: String? = fragment?.tag,
    container: Int = R.id.container
) {
    if (fragment == null) return
    val manager = supportFragmentManager
    val ft: FragmentTransaction = manager.beginTransaction()
    if (addToBackStack) {
        ft.replace(container, fragment, tag)
        ft.addToBackStack(tag)
        ft.commitAllowingStateLoss()
    } else {
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        ft.replace(container, fragment, tag)
        ft.commit()
    }
}
