package com.yudoo.extensions

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.yudoo.R
import com.yudoo.utils.NetworkUtils

fun Fragment.isInternetNotAvailable(): Boolean {
    return !NetworkUtils.isInternetAvailable(context)
}

fun Fragment.showToast(message: String?) {
    if (message.isNullOrEmpty() || context == null) return
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    /*val toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT)
    val view: View? = toast.view
    toast.setGravity(Gravity.CENTER, 0, 0)

    //To change the Background of Toast
    val text: TextView? = view?.findViewById(android.R.id.message)
    view?.setBackgroundResource(R.drawable.drawable_transparent_black_rounded)
    text?.gravity = Gravity.CENTER
    //Shadow of the Of the Text Color
    text?.setShadowLayer(0f, 0f, 0f, Color.TRANSPARENT)
    text?.setTextColor(Color.WHITE)
    toast.show()*/
}

fun Fragment.replaceFragments(
    fragment: Fragment?,
    addToBackStack: Boolean,
    tag: String? = fragment?.tag,
    container: Int = R.id.container
) {
    if (fragment == null) return
    val manager = fragmentManager ?: return
    val ft: FragmentTransaction = manager.beginTransaction()
    if (addToBackStack) {
        ft.replace(container, fragment, tag)
        ft.addToBackStack(tag)
        ft.commitAllowingStateLoss()
    } else {
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        ft.replace(container, fragment, tag)
        ft.commit()
    }
}