package com.yudoo.utils

import android.text.TextUtils
import java.text.SimpleDateFormat
import java.util.*

class DateTimeUtils {

    companion object {
        private val TAG = DateTimeUtils::class.java.simpleName

        val DATE_FORMAT = "dd MMM YYYY"
        val TIME_FORMAT = "hh:mm a"


        @JvmStatic
        fun getInstance(): DateTimeUtils {
            return DateTimeUtils()
        }
    }

    fun getTimeInMillisFromDate(
        date: String?,
        format: String? = DATE_FORMAT,
        locale: Locale = Locale.getDefault()
    ): Long {
        return if (!date.isNullOrEmpty() && !format.isNullOrEmpty()) {
            val sdf = SimpleDateFormat(format, locale)
            try {
                sdf.parse(date)?.time ?: System.currentTimeMillis()
            } catch (e: Exception) {
                System.currentTimeMillis()
            }
        } else {
            System.currentTimeMillis()
        }
    }

    @JvmOverloads
    fun format(
        timeInMillis: Long,
        format: String = DATE_FORMAT,
        locale: Locale = Locale.getDefault()
    ): String {
        return if (!TextUtils.isEmpty(format)) {
            val sdf = SimpleDateFormat(format, locale)
            sdf.format(timeInMillis)
        } else {
            Date(timeInMillis).toString()
        }
    }

    fun milliSecondsToTimer(milliseconds: Long): String {
        var finalTimerString = ""
        val minutesString: String
        val secondsString: String

        // Convert total duration into time
        val hours = (milliseconds / (1000 * 60 * 60)).toInt()
        val minutes = (milliseconds % (1000 * 60 * 60)).toInt() / (1000 * 60)
        val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
        // Add hours if there
        if (hours > 0) {
            finalTimerString = "$hours:"
        }

        // Prepending 0 to seconds if it is one digit
        minutesString = if (minutes < 10) {
            "0$minutes"
        } else {
            "" + minutes
        }

        // Prepending 0 to seconds if it is one digit
        secondsString = if (seconds < 10) {
            "0$seconds"
        } else {
            "" + seconds
        }

        finalTimerString = "$finalTimerString$minutesString:$secondsString"

        // return timer string
        return finalTimerString
    }

    fun getProgressPercentage(currentDuration: Long, totalDuration: Long): Int {
        val percentage: Double

        val currentSeconds = (currentDuration / 1000).toInt().toLong()
        val totalSeconds = (totalDuration / 1000).toInt().toLong()

        // calculating percentage
        percentage = currentSeconds.toDouble() / totalSeconds * 100

        // return percentage
        return percentage.toInt()
    }

}