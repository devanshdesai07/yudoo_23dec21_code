package com.yudoo.utils

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.yudoo.R

class SpannableUtils {

    fun setClickableTextSpan(
        textView: TextView,
        clickableText: ArrayList<String>?,
        @ColorRes color: Int = R.color.colorSecondary,
        clickListener: OnClickSpannableListener? = null
    ) {
        val text = textView.text.toString()

        if (clickableText.isNullOrEmpty()) {
            return
        }

        val spannableString = SpannableString(text)

        for (str in clickableText) {
            val start = text.lastIndexOf(str)
            if (start == -1) {
                continue
            }
            val end = start + str.length
            spannableString.setSpan(object : ClickableSpan() {
                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                    ds.color = ContextCompat.getColor(textView.context, color)
                }

                override fun onClick(widget: View) {
                    clickListener?.onSpannableClick(textView, str)
                }
            }, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(
                StyleSpan(Typeface.BOLD),
                start,
                end,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        textView.text = spannableString
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    fun setColorSpan(
        textView: TextView,
        colorText: ArrayList<String>?,
        color: Int
    ) {
        val text = textView.text.toString()

        if (colorText == null) {
            return
        }

        val spannableString = SpannableString(text)

        for (str in colorText) {
            val start = text.lastIndexOf(str)
            if (start == -1) {
                continue
            }
            val end = start + str.length
            spannableString.setSpan(
                ForegroundColorSpan(color),
                start,
                end,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        textView.text = spannableString
    }

    fun setColorSpan(
        textView: TextView,
        inputText: String?,
        color: Int = ContextCompat.getColor(textView.context, R.color.colorPrimary)
    ) {
        val text = textView.text.toString()

        if (TextUtils.isEmpty(inputText)) {
            return
        }

        val spannableString = SpannableString(text)

        val start = text.lastIndexOf(inputText!!)
        if (start == -1) {
            return
        }
        val end = start + inputText.length
        spannableString.setSpan(
            ForegroundColorSpan(color),
            start,
            end,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        /*spannableString.setSpan(
            StyleSpan(Typeface.BOLD),
            start,
            end,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )*/
        textView.text = spannableString
    }

    fun getForegroundColorSpan(
        context: Context,
        text: String?,
        textToColor: String?,
        @ColorRes color: Int = R.color.colorPrimary
    ): SpannableString? {

        if (text.isNullOrEmpty() || textToColor.isNullOrEmpty()) {
            return null
        }
        val spannableString = SpannableString(text)

        val start = text.lastIndexOf(textToColor)
        if (start == -1) {
            return null
        }
        val end = start + textToColor.length
        spannableString.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, color)),
            start,
            end,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannableString
    }

    fun getRelativeSizeSpan(
        text: String?,
        textToChange: String?,
        sizeSpan: Float = 1f
    ): SpannableString? {
        if (text.isNullOrEmpty() || textToChange.isNullOrEmpty()) {
            return null
        }

        val spannableString = SpannableString(text)

        val start = text.lastIndexOf(textToChange)
        if (start == -1) {
            return null
        }
        val end = start + textToChange.length
        spannableString.setSpan(
            RelativeSizeSpan(sizeSpan),
            start,
            end,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannableString
    }

    interface OnClickSpannableListener {
        fun onSpannableClick(view: View, str: String)
    }

    companion object {
        fun newInstance(): SpannableUtils {
            return SpannableUtils()
        }
    }

}