package com.yudoo.utils

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.google.gson.Gson
import com.yudoo.AppClass
import com.yudoo.model.registration.ShopOwnerData


class PreferenceHelper() {
    private var sharedPreferences: SharedPreferences? = null
    private val preferenceName = "sp_trade_bin"

    /**
     * get shared preference
     *
     * @return
     */
    private val preferences: SharedPreferences
        get() {
            if (sharedPreferences == null) {
                sharedPreferences =
                    AppClass.getInstance()
                        .getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
            }
            return sharedPreferences!!
        }

    companion object {
        private const val IS_LOGIN = "IS_LOGIN"
        private const val FCM_TOKEN = "FCM_TOKEN"
        private const val LOGIN_DATA = "LOGIN_DATA"
        private const val IS_ON_BOARDING_SHOWN = "IS_ON_BOARDING_SHOWN"
        private const val IS_GST_DETAILS_FILLED = "IS_GST_DETAILS_FILL"

        private var preference: PreferenceHelper? = null

        fun getInstance(): PreferenceHelper {
            if (preference == null) {
                preference = PreferenceHelper()
            }
            return preference as PreferenceHelper
        }
    }

    /**
     * get shared preference editor
     *
     * @return
     */
    private val editor: SharedPreferences.Editor
        get() = preferences.edit()

    /**
     * check preference contain key
     *
     * @param key preference key
     * @return return true if preference contain key otherwise false
     */
    fun contain(key: String): Boolean {
        return preferences.contains(key)
    }


    /********************************************************
     * get methods
     */

    /**
     * get string preference value
     *
     * @param key preference key
     * @return return value for key or default value empty string
     */
    fun getString(key: String): String {
        return preferences.getString(key, "")!!
    }

    /**
     * get string preference value
     *
     * @param key preference key
     * @return return value for key or default value empty string
     */
    fun getString(key: String, default: String): String {
        return preferences.getString(key, default)!!
    }

    /**
     * get integer preference value
     *
     * @param key preference key
     * @return return value for key or default value 0
     */
    fun getInt(key: String): Int {
        return preferences.getInt(key, 0)
    }

    /**
     * get boolean preference value
     *
     * @param key preference key
     * @return return value for key or default value false
     */
    fun getBoolean(key: String): Boolean {
        return preferences.getBoolean(key, false)
    }

    /**
     * get boolean preference value
     *
     * @param key preference key
     * @param default default value
     * @return return value for key or default value false
     */
    private fun getBoolean(key: String, default: Boolean): Boolean {
        return preferences.getBoolean(key, default)
    }

    /**
     * get float preference value
     *
     * @param key preference key
     * @return return value for key or default value 0
     */
    fun getFloat(key: String): Float {
        return preferences.getFloat(key, 0f)
    }

    /**
     * get double preference value
     *
     * @param key preference key
     * @return return value for key or default value 0
     */
    fun getDouble(key: String): Double {
        return java.lang.Double.parseDouble(preferences.getString(key, "0")!!)
    }


    /**
     * get long preference value
     *
     * @param key preference key
     * @return return value for key or default value 0
     */
    fun getLong(key: String): Long {
        return preferences.getLong(key, 0)
    }

    /********************************************************
     * set methods
     */

    /**
     * put string value
     *
     * @param key   preference key
     * @param value value for key
     */
    fun setString(key: String, value: String) {
        editor.putString(key, value).apply()
    }

    /**
     * put int value
     *
     * @param key   preference key
     * @param value value for key
     */
    fun setInt(key: String, value: Int) {
        editor.putInt(key, value).apply()
    }

    /**
     * put boolean value
     *
     * @param key   preference key
     * @param value value for key
     */
    fun setBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value).apply()
    }

    /**
     * put float value
     *
     * @param key   preference key
     * @param value value for key
     */
    fun setFloat(key: String, value: Float) {
        editor.putFloat(key, value).apply()
    }

    /**
     * put float value
     *
     * @param key   preference key
     * @param value value for key
     */
    fun setDouble(key: String, value: Double) {
        editor.putString(key, value.toString()).apply()
    }

    /**
     * put long value
     *
     * @param key   preference key
     * @param value value for key
     */
    fun setLong(key: String, value: Long) {
        editor.putLong(key, value).apply()
    }

    fun clear() {
        editor.clear().commit()
    }

    var isLogin: Boolean
        get() = getBoolean(IS_LOGIN, false)
        set(login) {
            setBoolean(IS_LOGIN, login)
        }

    var isOnBoardingShown: Boolean
        get() = getBoolean(IS_ON_BOARDING_SHOWN, false)
        set(isOnBoardingShown) {
            setBoolean(IS_ON_BOARDING_SHOWN, isOnBoardingShown)
        }

    var isGstDetailsFilled: Boolean
        get() = getBoolean(IS_GST_DETAILS_FILLED, false)
        set(isGstDetailsFilled) {
            setBoolean(IS_GST_DETAILS_FILLED, isGstDetailsFilled)
        }


    var fcmToken: String?
        get() = getString(FCM_TOKEN, "")
        set(token) {
            setString(FCM_TOKEN, token ?: "")
        }

    var registrationData: ShopOwnerData?
        get() {
            val value = getString(LOGIN_DATA)
            return if (!TextUtils.isEmpty(value)) {
                Gson().fromJson(value, ShopOwnerData::class.java)
            } else {
                null
            }
        }
        set(response) {
            val value = Gson().toJson(response)
            setString(LOGIN_DATA, value)
        }

}
