package com.yudoo.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.text.InputFilter
import android.text.TextUtils
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import java.util.regex.Pattern

object AppUtils {
    private val TAG = AppUtils::class.java.simpleName
    fun isValidEmail(email: String?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun getJoinedString(delimiter: String = ", ", vararg dataList: String?): String {
        val filterList = ArrayList<String>()

        dataList.forEach { data ->
            if (!data.isNullOrEmpty()) {
                filterList.add(data)
            }
        }
        return TextUtils.join(delimiter, filterList)
    }

    fun getJoinedString(delimiter: String = ", ", dataList: ArrayList<String?>?): String {
        val filterList = ArrayList<String>()
        dataList?.forEach { data ->
            if (!data.isNullOrEmpty()) {
                filterList.add(data)
            }
        }
        return TextUtils.join(delimiter, filterList)
    }

    fun getColorFromString(colorStr: String? = "#000000"): Int {
        colorStr ?: return 0
        var colorName = colorStr
        if (!colorStr.contains("#")) {
            colorName = "#$colorName"
        }
        return try {
            Color.parseColor(colorName)
        } catch (e: Exception) {
            0
        }
    }

    fun hideSoftKeyboard(activity: Activity) {
        val focusedView = activity.currentFocus
        if (focusedView != null) {
            val inputMethodManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(focusedView.windowToken, 0)
        }
    }

    fun showSoftKeyboard(activity: Activity) {
        val focusedView = activity.currentFocus
        if (focusedView != null) {
            val inputMethodManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(focusedView, 0)
        }
    }

    fun doNotAllowedSmileyAndSpecialCharFilter(canAllowSpace: Boolean?): InputFilter {
        return InputFilter { source, start, end, dest, dstart, dend ->
            for (i in start until end) {
                if (source.toString().matches(Regex("[a-zA-Z0-9 ,._]+"))) {
                    return@InputFilter if (!canAllowSpace!! && Character.isSpaceChar(source[i])) {
                        ""
                    } else {
                        source
                    }
                }
            }
            ""
        }
    }

    fun isDigit(text: String): Boolean {
        var isDigit = true
        if (!TextUtils.isEmpty(text)) {
            for (element in text) {
                if (!Character.isDigit(element)) {
                    isDigit = false
                    break
                }
            }
        } else {
            isDigit = false
        }
        return isDigit
    }

    fun isNumber(text: String?): Boolean {
        if (!TextUtils.isEmpty(text)) {
            val pattern = Pattern.compile("^(([0-9]*)|(([0-9]*)\\.([0-9]*)))$")
            return pattern.matcher(text).matches()
        }
        return false
    }

    fun isValidPan(text: String?): Boolean {
        if (!TextUtils.isEmpty(text)) {
            val pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}")
            return pattern.matcher(text).matches()
        }
        return false
    }

    fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0
        var locationProviders: String
        locationMode = try {
            Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
        } catch (e: SettingNotFoundException) {
            e.printStackTrace()
            return false
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF
    }
}