package com.yudoo.utils

import java.util.Observable

internal class NotificationObservable : Observable() {

    override fun notifyObservers(data: Any) {
        setChanged()
        super.notifyObservers(data)
    }
}