package com.yudoo.utils

import java.util.*

class NotificationCenter private constructor() {
    private val observables: HashMap<String, NotificationObservable> = HashMap()

    companion object {

        private var instance: NotificationCenter? = null

        @Synchronized
        fun getInstance(): NotificationCenter {
            if (instance == null) {
                instance = NotificationCenter()
            }
            return instance as NotificationCenter
        }
    }

    fun addObserver(notification: String, observer: Observer?) {
        if (observer == null) return
        var observable = observables[notification]
        if (observable == null) {
            observable = NotificationObservable()
            observables[notification] = observable
        }
        observable.addObserver(observer)
    }

    fun removeObserver(notification: String, observer: Observer?) {
        val observable = observables[notification]
        observable?.deleteObserver(observer)
    }

    fun postNotification(notification: String, item: Any = "") {
        val observable = observables[notification]
        observable?.notifyObservers(item)
    }

    fun getObserverCount(notification: String): Int {
        val observable = observables[notification]
        return observable?.countObservers() ?: 0
    }

}