package com.yudoo.api

import com.yudoo.model.gst.GstDetailsResponse
import com.yudoo.model.registration.ShopOwnerData
import com.yudoo.model.registration.ShopOwnerResponse
import com.yudoo.model.splash.CheckVersionResponseModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface WebAPIService {

//    POST /api/shopOwner/ShopOwner_All
//    POST /api/shopOwner/ShopOwner_ById
//    POST /api/shopOwner/CheckSeller_Exists
//    POST /api/shopOwner/CheckSeller_Exists_ById

//    POST /api/shopOwner/ShopOwner_Upsert
//    POST /api/shopOwner/CheckSeller_Exists_ByMobileNumberAndById
//    GET /api/shopOwner/Users_SendOTP
//    GET /api/shopOwner/GetGSTInfo


    @GET("api/shopOwner/GetGSTInfo")
    fun getGstNoDetails(
        @Query("gstnumber") gstNumber: String
    ): Call<GstDetailsResponse>

    @GET("api/shopOwner/Users_SendOTP")
    fun sendOtpOnMobile(
        @Query("mobilenumber") mobileNumber: String
    ): Call<String>

    @POST("api/shopOwner/CheckSeller_Exists_ByMobileNumberAndById")
    fun checkSellerExistByMobileAndId(
        @Query("Id") shopId: String,
        @Query("Mobilenumber") mobile: String
    ): Call<ShopOwnerResponse>


    @POST("api/shopOwner/CheckSeller_Exists")
    fun checkSellerExist(
        @Query("ShopOwnerName") shopOwnerName: String?,
        @Query("ShopName") shopName: String?,
        @Query("MobileNumber") mobileNumber: String?
    ): Call<ShopOwnerResponse>


    @POST("api/shopOwner/ShopOwner_Upsert")
    fun registerOrUpdateShopOwner(
        @Body request: ShopOwnerData?
    ): Call<ShopOwnerResponse>

    @POST("api/version/CheckForUpdate")
    fun checkForUpdate(
        @Query("IsAndroid") IsAndroid: Boolean?
    ): Call<CheckVersionResponseModel>

}