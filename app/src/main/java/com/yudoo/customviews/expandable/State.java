package com.yudoo.customviews.expandable;

public interface State {
    int COLLAPSED = 0;
    int COLLAPSING = 1;
    int EXPANDING = 2;
    int EXPANDED = 3;
}
