package com.yudoo.customviews.expandable;

import android.animation.Animator;

class ExpansionListener implements Animator.AnimatorListener {
    private final ExpandableLayout expandableLayout;
    private int targetExpansion;
    private boolean canceled;

    public ExpansionListener(ExpandableLayout expandableLayout, int targetExpansion) {
        this.expandableLayout = expandableLayout;
        this.targetExpansion = targetExpansion;
    }

    @Override
    public void onAnimationStart(Animator animation) {
        expandableLayout.state = targetExpansion == 0 ? State.COLLAPSING : State.EXPANDING;
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        if (!canceled) {
            expandableLayout.state = targetExpansion == 0 ? State.COLLAPSED : State.EXPANDED;
            expandableLayout.setExpansion(targetExpansion);
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        canceled = true;
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }
}
