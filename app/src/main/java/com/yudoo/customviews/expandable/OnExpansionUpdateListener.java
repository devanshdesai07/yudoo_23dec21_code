package com.yudoo.customviews.expandable;

public interface OnExpansionUpdateListener {
    /**
     * Callback for expansion updates
     *
     * @param expansionFraction Value between 0 (collapsed) and 1 (expanded) representing the the expansion progress
     * @param state             One of {@link State} repesenting the current expansion state
     */
    void onExpansionUpdate(float expansionFraction, int state);
}
