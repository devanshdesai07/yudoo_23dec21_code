package com.yudoo.customviews.otp;

public interface OnOtpCompletionListener {
    void onOtpCompleted(String otp);

    void onOtpType(String otp);
}
