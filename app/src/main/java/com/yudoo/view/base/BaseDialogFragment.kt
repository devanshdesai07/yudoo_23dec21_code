package com.yudoo.view.base

import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.yudoo.view.dialog.CustomDialog

abstract class BaseDialogFragment : DialogFragment() {

    companion object {
        private val TAG = BaseDialogFragment::class.java.simpleName
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        );
    }

    fun show(manager: FragmentManager) {
        manager.beginTransaction().add(this, tag).commitAllowingStateLoss()
    }

    private var progressDialog: CustomDialog? = null

    open fun showProgressDialog() {
        progressDialog = CustomDialog(requireContext())
        progressDialog?.show()
    }

    open fun hideProgressDialog() {
        progressDialog?.dismiss()
    }
}
