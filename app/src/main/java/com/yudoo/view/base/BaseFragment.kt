package com.yudoo.view.base

import androidx.fragment.app.Fragment
import com.yudoo.view.dialog.CustomDialog


abstract class BaseFragment : Fragment() {

    private var progressDialog: CustomDialog? = null

    open fun showProgressDialog() {
        progressDialog = CustomDialog(requireContext())
        progressDialog?.show()
    }

    open fun hideProgressDialog() {
        progressDialog?.dismiss()
    }

}
