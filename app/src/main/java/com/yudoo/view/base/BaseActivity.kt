package com.yudoo.view.base

import android.content.pm.PackageManager
import android.content.pm.PackageManager.GET_META_DATA
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.yudoo.view.dialog.CustomDialog


abstract class BaseActivity : AppCompatActivity() {

    private var progressDialog: CustomDialog? = null

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    open fun showProgressDialog() {
        progressDialog = CustomDialog(this)
        progressDialog?.show()
    }

    open fun hideProgressDialog() {
        progressDialog?.dismiss()
    }

}
