package com.yudoo.view.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.yudoo.R
import com.yudoo.api.WebAPIServiceFactory
import com.yudoo.databinding.ActivityRegisterOrLoginBinding
import com.yudoo.extensions.setToolbar
import com.yudoo.extensions.showToast
import com.yudoo.model.registration.ShopOwnerData
import com.yudoo.repository.UserRepository
import com.yudoo.utils.AppUtils
import com.yudoo.utils.PreferenceHelper
import com.yudoo.view.base.BaseActivity
import com.yudoo.view.home.HomeActivity
import com.yudoo.viewmodel.UserViewModel
import com.yudoo.viewmodel.UserViewModelFactory
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

class RegisterOrLoginActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityRegisterOrLoginBinding
    private var mOtp: String? = null

    private val decimalFormat = DecimalFormat("00")
    private val mResendTime: Long = TimeUnit.SECONDS.toMillis(30)
    private var mCountDownTimer: CountDownTimer? = null

    private val userViewModel: UserViewModel by viewModels {
        UserViewModelFactory(
            UserRepository(
                WebAPIServiceFactory.newInstance().makeServiceFactory()
            )
        )
    }

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, RegisterOrLoginActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterOrLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {
        setToolbar(binding.appBar.toolbar, R.string.app_name)

        binding.rgRegistrationOption.setOnCheckedChangeListener { _, checkedId ->
            hideShowRegistrationViews()
        }
        // By Default
        binding.rgRegistrationOption.check(R.id.rbNotRegistered)

        binding.btnEnter.setOnClickListener(this)
        binding.btnChangeNo.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        binding.btnConfirm.setOnClickListener(this)
        binding.tvResend.setOnClickListener(this)


        // hide show progress
        userViewModel.loadingStatus.observe(this, {
            it?.let {
                if (it) {
                    showProgressDialog()
                } else {
                    hideProgressDialog()
                }
            }
        })

        // show error message
        userViewModel.errorMessage.observe(this, {
            it?.let { showToast(it) }
        })

        // seller exist data (Login)
        userViewModel.sellerExistData.observe(this, {
            it?.let {

                Log.e("Data", "${it.Item==null} = ${it.Item?.ShopName} = ${it.Message}")

                if (it.Item != null) {
                    // Seller Exist
                    PreferenceHelper.getInstance().isLogin = true
                    PreferenceHelper.getInstance().registrationData = it.Item
                    if (it.Item?.GstNumber == null) {
                        PreferenceHelper.getInstance().isGstDetailsFilled = false
                        GstRegistrationActivity.start(this)
                    } else {
                        PreferenceHelper.getInstance().isGstDetailsFilled = true
                        HomeActivity.start(this)
                    }
                } else {
                    showToast("Registration Failed")
                }
            }
        })

        // seller not exist data (Registration)
        userViewModel.sellerNotExistData.observe(this, {
            PreferenceHelper.getInstance().isLogin = true
            PreferenceHelper.getInstance().isGstDetailsFilled = false
            PreferenceHelper.getInstance().registrationData = it
            GstRegistrationActivity.start(this)
        })

        // login data
        userViewModel.loginData.observe(this, {
            it?.let {
                if (it.Item != null) {
                    PreferenceHelper.getInstance().isLogin = true
                    PreferenceHelper.getInstance().isGstDetailsFilled = true
                    PreferenceHelper.getInstance().registrationData = it.Item
                    HomeActivity.start(this)
                } else {
                    showToast("Login Failed")
                }
            }
        })

        userViewModel.otpData.observe(this, {
            it?.let {
                startCountDown()
                mOtp = it
            }
        })

        /*binding.otpView.setOtpCompletionListener(object : OnOtpCompletionListener {
            override fun onOtpCompleted(otp: String?) {
            }

            override fun onOtpType(otp: String?) {
                binding.btnConfirm.isEnabled = otp?.length == 4
            }
        })*/
    }

    override fun onDestroy() {
        stopCountDown()
        super.onDestroy()
    }

    private fun startCountDown() {
        if (mCountDownTimer == null) {
            binding.tvResend.visibility = View.GONE
            binding.tvTimer.visibility = View.VISIBLE
            mCountDownTimer = object : CountDownTimer(mResendTime, 1000) {
                override fun onFinish() {
                    binding.tvResend.visibility = View.VISIBLE
                    binding.tvTimer.visibility = View.GONE
                    mCountDownTimer = null
                }

                override fun onTick(millisUntilFinished: Long) {
                    binding.tvTimer.text = decimalFormat.format(millisUntilFinished / 1000)
                }
            }.start()
        }
    }

    private fun stopCountDown() {
        mCountDownTimer?.cancel()
        mCountDownTimer = null
    }

    private fun hideShowRegistrationViews() {
        when (binding.rgRegistrationOption.checkedRadioButtonId) {
            R.id.rbRegistered -> {
                binding.cardRegistered.isVisible = true
                binding.btnSubmit.isVisible = true

                binding.cardNotRegistered.isVisible = false
                binding.btnEnter.isVisible = false
                binding.btnChangeNo.isVisible = false
            }
            R.id.rbNotRegistered -> {
                binding.cardRegistered.isVisible = false
                binding.btnSubmit.isVisible = false

                binding.cardNotRegistered.isVisible = true
                binding.btnEnter.isVisible = true
            }
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnEnter -> {
                if (isRegistrationValid()) {
                    AppUtils.hideSoftKeyboard(this)
                    binding.llOtp.isVisible = true
                    binding.tvOtpDescription.text =
                        getString(R.string.enter_the_otp_sent_to, binding.edtMobile.text.toString())

                    userViewModel.sendOtpOnMobile(binding.edtMobile.text.toString().trim())
                }
            }
            R.id.btnSubmit -> {
                if (isLoginValid()) {
                    AppUtils.hideSoftKeyboard(this)
                    binding.llOtp.isVisible = true
                    binding.tvOtpDescription.text = getString(
                        R.string.enter_the_otp_sent_to,
                        binding.edtMobileLogin.text.toString()
                    )

                    userViewModel.sendOtpOnMobile(binding.edtMobileLogin.text.toString().trim())
                }
            }
            R.id.tvResend -> {
                if (binding.rgRegistrationOption.checkedRadioButtonId == R.id.rbNotRegistered) {
                    userViewModel.sendOtpOnMobile(binding.edtMobile.text.toString().trim())
                } else {
                    userViewModel.sendOtpOnMobile(binding.edtMobileLogin.text.toString().trim())
                }
            }
            R.id.btnChangeNo -> {
                //binding.llOtp.isVisible = false
            }
            R.id.btnConfirm -> {
                if (binding.otpView.text.isNullOrEmpty()) {
                    showToast("Please enter OTP")
                    return
                }
                if (mOtp == binding.otpView.text.toString()) {

                    // Check Seller Exist or Not if Exist then redirect to login other wise in GST screen
                    if (binding.rgRegistrationOption.checkedRadioButtonId == R.id.rbNotRegistered) {
                        val request = ShopOwnerData().apply {
                            ShopOwnerName = binding.edtYourName.text.toString().trim()
                            ShopName = binding.edtShopName.text.toString().trim()
                            MobileNumber = binding.edtMobile.text.toString().trim()
                            IsRegister = true
                        }
                        userViewModel.checkSellerExist(request);
                    } else {
                        userViewModel.checkSellerExistByMobileAndId(
                            binding.edtShopId.text.toString().trim(),
                            binding.edtMobileLogin.text.toString().trim()
                        )
                    }

                } else {
                    showToast("Invalid OTP. Try again!")
                }
            }
        }
    }

    private fun isRegistrationValid(): Boolean {
        if (binding.edtYourName.text.isNullOrEmpty()) {
            showToast("Please enter your name")
            return false
        }
        if (binding.edtShopName.text.isNullOrEmpty()) {
            showToast("Please enter shop name")
            return false
        }
        if (binding.edtMobile.text.isNullOrEmpty()) {
            showToast("Please enter mobile number")
            return false
        }
        if ((binding.edtMobile.text?.length ?: 0) < 8) {
            showToast("Please enter valid mobile number")
            return false
        }
        return true
    }


    private fun isLoginValid(): Boolean {
        if (binding.edtShopId.text.isNullOrEmpty()) {
            showToast("Please enter shop Id")
            return false
        }
        if (binding.edtMobileLogin.text.isNullOrEmpty()) {
            showToast("Please enter mobile number")
            return false
        }
        if ((binding.edtMobileLogin.text?.length ?: 0) < 8) {
            showToast("Please enter valid mobile number")
            return false
        }
        return true
    }

}