package com.yudoo.view.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.yudoo.R
import com.yudoo.api.WebAPIServiceFactory
import com.yudoo.databinding.ActivityGstRegistrationBinding
import com.yudoo.extensions.setToolbar
import com.yudoo.extensions.showToast
import com.yudoo.listener.DelayTextWatcher
import com.yudoo.model.registration.ShopOwnerData
import com.yudoo.repository.UserRepository
import com.yudoo.utils.PreferenceHelper
import com.yudoo.view.base.BaseActivity
import com.yudoo.view.home.HomeActivity
import com.yudoo.viewmodel.UserViewModel
import com.yudoo.viewmodel.UserViewModelFactory

class GstRegistrationActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityGstRegistrationBinding

    private val userViewModel: UserViewModel by viewModels {
        UserViewModelFactory(
            UserRepository(
                WebAPIServiceFactory.newInstance().makeServiceFactory()
            )
        )
    }

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, GstRegistrationActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGstRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {
        setToolbar(binding.appBar.toolbar, R.string.app_name)

        binding.rgRegistrationOption.setOnCheckedChangeListener { _, checkedId ->
            hideShowRegistrationViews()
        }
        // By Default
        binding.rgRegistrationOption.check(R.id.rbGstRegistered)

        binding.btnNext.setOnClickListener(this)

        binding.edtGstNumber.addTextChangedListener(object : DelayTextWatcher(200) {
            override fun onAfterTextChanged() {
                if (binding.edtGstNumber.length() == resources.getInteger(R.integer.gst_length)) {
                    userViewModel.getGstNoDetails(binding.edtGstNumber.text.toString())
                }
            }
        })

        // hide show progress gst
        userViewModel.gstLoadingStatus.observe(this, {
            it?.let {
                binding.progressBarGst.isVisible = it
            }
        })
        userViewModel.gstDetailsData.observe(this, {
            binding.edtCompanyPanNumber.setText(it.taxpayerInfo?.panNo ?: "")
            binding.edtRegisteredAddress.setText(it.taxpayerInfo?.pradr?.addr?.getFormattedAddress())
        })


        // hide show progress
        userViewModel.loadingStatus.observe(this, {
            it?.let {
                if (it) {
                    showProgressDialog()
                } else {
                    hideProgressDialog()
                }
            }
        })

        // show error message
        userViewModel.errorMessage.observe(this, {
            it?.let { showToast(it) }
        })

        // registration data
        userViewModel.sellerExistData.observe(this, {
            it?.let {
                PreferenceHelper.getInstance().isLogin = true
                PreferenceHelper.getInstance().isGstDetailsFilled = true
                PreferenceHelper.getInstance().registrationData = it.Item
                HomeActivity.start(this)
            }
        })

    }

    private fun hideShowRegistrationViews() {
        when (binding.rgRegistrationOption.checkedRadioButtonId) {
            R.id.rbGstRegistered -> {
                binding.cardRegistered.isVisible = true
                binding.cardNotRegistered.isVisible = false
            }
            R.id.rbGstNotRegistered -> {
                binding.cardRegistered.isVisible = false
                binding.cardNotRegistered.isVisible = true
            }
        }
    }

    private fun isValidRegistered(): Boolean {
        if (binding.edtGstNumber.text.isNullOrEmpty()) {
            showToast("Please enter GST number")
            return false
        }
        if (binding.edtGstNumber.text.toString().length < 15) {
            showToast("Please enter valid GST number")
            return false
        }
        if (binding.edtRegisteredAddress.text.isNullOrEmpty()) {
            showToast("Please enter registered address")
            return false
        }
        if (binding.edtCompanyPanNumber.text.isNullOrEmpty()) {
            showToast("Please enter company pan number")
            return false
        }
        /*if (AppUtils.isValidPan(binding.edtCompanyPanNumber.text.toString())) {
            showToast("Please enter valid company pan number")
            return false
        }*/
        if (binding.edtCompanyPanNumber.text.toString().length < 10) {
            showToast("Please enter valid company pan number")
            return false
        }
        if (binding.edtPersonalPanNumber.text.isNullOrEmpty()) {
            showToast("Please enter personal pan number")
            return false
        }
        if (binding.edtPersonalPanNumber.text.toString().length < 10) {
            showToast("Please enter valid personal pan number")
            return false
        }
        /*if (AppUtils.isValidPan(binding.edtPersonalPanNumber.text.toString())) {
            showToast("Please enter valid personal pan number")
            return false
        }*/
        return true
    }


    private fun isValidNotRegistered(): Boolean {
        if (binding.edtPersonalPanNumberNotReg.text.isNullOrEmpty()) {
            showToast("Please enter personal pan number")
            return false
        }
        if (binding.edtPersonalPanNumberNotReg.text.toString().length < 10) {
            showToast("Please enter valid personal pan number")
            return false
        }
        return true
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnNext -> {
                if (binding.rgRegistrationOption.checkedRadioButtonId == R.id.rbGstNotRegistered) {
                    if (isValidNotRegistered()) {
                        val request = ShopOwnerData().apply {
                            Id = PreferenceHelper.getInstance().registrationData?.Id.toString()
                            ShopName =
                                PreferenceHelper.getInstance().registrationData?.ShopName.toString()
                            ShopOwnerName =
                                PreferenceHelper.getInstance().registrationData?.ShopOwnerName.toString()
                            MobileNumber =
                                PreferenceHelper.getInstance().registrationData?.MobileNumber.toString()
                            PersonalPanNumber =
                                binding.edtPersonalPanNumberNotReg.text.toString().trim()
                        }
                        userViewModel.registerOrUpdateShopOwner(request)
                    }
                } else {
                    if (isValidRegistered()) {
                        val request = ShopOwnerData().apply {
                            Id = PreferenceHelper.getInstance().registrationData?.Id.toString()
                            ShopName =
                                PreferenceHelper.getInstance().registrationData?.ShopName.toString()
                            ShopOwnerName =
                                PreferenceHelper.getInstance().registrationData?.ShopOwnerName.toString()
                            MobileNumber =
                                PreferenceHelper.getInstance().registrationData?.MobileNumber.toString()
                            GstNumber = binding.edtGstNumber.text.toString().trim()
                            CompanyRegisterAddress =
                                binding.edtRegisteredAddress.text.toString().trim()
                            CompanyPanNumber = binding.edtCompanyPanNumber.text.toString().trim()
                            PersonalPanNumber = binding.edtPersonalPanNumber.text.toString().trim()
                        }
                        userViewModel.registerOrUpdateShopOwner(request)
                    }
                }
            }
        }
    }

}