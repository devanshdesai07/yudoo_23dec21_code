package com.yudoo.view.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.Window;

import com.yudoo.R;


public class CustomDialog extends ProgressDialog {

    public CustomDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setMessage("Please wait...");
        setContentView(R.layout.custom_progress_dialog);
        setCancelable(false);
    }
}
