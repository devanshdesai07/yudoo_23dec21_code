package com.yudoo.view.splash

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.gson.Gson
import com.yudoo.R
import com.yudoo.api.RemoteCallback
import com.yudoo.api.WebAPIServiceFactory
import com.yudoo.databinding.ActivitySplashBinding
import com.yudoo.model.splash.CheckVersionResponseModel
import com.yudoo.utils.PreferenceHelper
import com.yudoo.view.home.HomeActivity
import com.yudoo.view.onboarding.OnBoardingActivity
import com.yudoo.view.register.GstRegistrationActivity
import com.yudoo.view.register.RegisterOrLoginActivity
import com.yudoo.viewmodel.SplashViewModel
import com.yudoo.viewmodel.SplashViewModelFactory

class SplashActivity : AppCompatActivity() {

    private val TAG = "SplashActivity"
    lateinit var binding: ActivitySplashBinding

    private val splashViewModel: SplashViewModel by viewModels {
        SplashViewModelFactory(this.application)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        checkForUpdate()

    }

    private fun checkForUpdate() {
        WebAPIServiceFactory.newInstance().makeServiceFactory().checkForUpdate(true).enqueue(object : RemoteCallback<CheckVersionResponseModel>() {
            override fun onSuccess(response: CheckVersionResponseModel?) {
                if (response!!.code == 200) {
                    if (response.item.versionName == "1.0") {
                        initView()
                    } else {
                        val builder = AlertDialog.Builder(this@SplashActivity, R.style.MyDialogTheme)
                        builder.setMessage(response.item.message)
                        builder.setCancelable(false)
                        builder.setPositiveButton("OK") { dialog, which ->
                            onBackPressed()
                        }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }
                }
            }

            override fun onFailed(throwable: Throwable) {
                Log.d(TAG, "onSuccess: " + Gson().toJson(throwable.message))
            }

            override fun onInternetFailed() {
                Log.d(TAG, "onSuccess: Internet Connection Failed")
            }

            override fun onComplete() {
                Log.d(TAG, "onSuccess: complete")
            }
        })
    }

    private fun initView() {
        splashViewModel.splashStatus.observe(this, {
            if (it) {
                if (PreferenceHelper.getInstance().isLogin) {
                    if (PreferenceHelper.getInstance().isGstDetailsFilled) {
                        HomeActivity.start(this)
                    } else {
                        GstRegistrationActivity.start(this)
                    }
                } else {
                    if (PreferenceHelper.getInstance().isOnBoardingShown) {
                        RegisterOrLoginActivity.start(this)
                    } else {
                        OnBoardingActivity.start(this)
                    }
                }
            }
        })
        splashViewModel.initSplashScreen()
    }
}