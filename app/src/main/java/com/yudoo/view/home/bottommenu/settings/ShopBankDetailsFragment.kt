package com.yudoo.view.home.bottommenu.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.adapter.TimeSelectionAdapter
import com.yudoo.databinding.FragmentShopBankDetailsBinding
import com.yudoo.model.ModelTimeSelection


class ShopBankDetailsFragment : Fragment(), View.OnClickListener,
    OnItemClickListener<ModelTimeSelection> {

    lateinit var binding: FragmentShopBankDetailsBinding
    private var adapterTimeSelection: TimeSelectionAdapter? = null

    companion object {

        @JvmStatic
        fun newInstance() = ShopBankDetailsFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentShopBankDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            
        }
    }

    override fun onItemClick(view: View?, item: ModelTimeSelection?, position: Int) {

    }

}