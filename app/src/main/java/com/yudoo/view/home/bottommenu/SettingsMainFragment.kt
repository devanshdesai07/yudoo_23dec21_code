package com.yudoo.view.home.bottommenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.yudoo.R
import com.yudoo.databinding.FragmentSettingsMainBinding
import com.yudoo.extensions.replaceFragments
import com.yudoo.view.home.bottommenu.settings.RatingsFragment
import com.yudoo.view.home.bottommenu.settings.ShopInfoFragment
import com.yudoo.view.home.bottommenu.settings.ShopUsersFragment
import com.yudoo.view.home.bottommenu.settings.SupportsFragment

class SettingsMainFragment : Fragment() {

    lateinit var binding: FragmentSettingsMainBinding
    //private var pagerAdapter: MyPagerAdapter? = null

    companion object {
        @JvmStatic
        fun newInstance() = SettingsMainFragment()
    }

    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }*/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSettingsMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {

        /*val fragmentList = ArrayList<ModelFragment>()
        fragmentList.add(
            ModelFragment(ShopInfoFragment.newInstance(), "Shop Info")
        )
        fragmentList.add(
            ModelFragment(ShopUsersFragment.newInstance(), "Shop Users")
        )
        fragmentList.add(
            ModelFragment(RatingsFragment.newInstance(), "Rating")
        )
        fragmentList.add(
            ModelFragment(SupportsFragment.newInstance(), "Support")
        )*/

        /*pagerAdapter = activity?.let { MyPagerAdapter(it, fragmentList) }
        binding.viewPager.adapter = pagerAdapter*/

        /*TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = fragmentList[position].title
        }.attach()*/


        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Shop Info"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Shop Users"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Rating"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Support"))


        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                onSelectTabNavigate(tab?.position ?: 0)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                onSelectTabNavigate(tab?.position ?: 0)
            }
        })

        // Select First Tab by default
        binding.tabLayout.selectTab(binding.tabLayout.getTabAt(0))
    }

    private fun onSelectTabNavigate(pos: Int) {
        when (pos) {
            0 -> replaceFragments(
                ShopInfoFragment.newInstance(),
                false,
                container = R.id.containerSetting
            )
            1 -> replaceFragments(
                ShopUsersFragment.newInstance(),
                false,
                container = R.id.containerSetting
            )
            2 -> replaceFragments(
                RatingsFragment.newInstance(),
                false,
                container = R.id.containerSetting
            )
            3 -> replaceFragments(
                SupportsFragment.newInstance(),
                false,
                container = R.id.containerSetting
            )
        }
    }
}