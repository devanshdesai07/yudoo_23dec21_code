package com.yudoo.view.home.bottommenu.callback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yudoo.R
import com.yudoo.adapter.CallHistoryAdapter
import com.yudoo.databinding.FragmentCallHistoryBinding
import com.yudoo.model.ModelCallHistory
import com.yudoo.utils.GridSpacingItemDecoration


class CallHistoryFragment : Fragment(), View.OnClickListener {

    lateinit var binding: FragmentCallHistoryBinding

    private var mAdapterCallHistory: CallHistoryAdapter? = null

    companion object {

        @JvmStatic
        fun newInstance() = CallHistoryFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentCallHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        mAdapterCallHistory = CallHistoryAdapter()
        binding.rvCallHistory.layoutManager = LinearLayoutManager(requireContext())
        binding.rvCallHistory.addItemDecoration(
            GridSpacingItemDecoration(
                1,
                R.dimen.size_8dp,
                true,
                0
            )
        )
        binding.rvCallHistory.adapter = mAdapterCallHistory

        setData()
    }

    private fun setData() {
        val callHistoryList = ArrayList<ModelCallHistory>()
        callHistoryList.add(ModelCallHistory("John Doe", "06 minutes", "1 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "14 minutes", "2 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "20 minutes", "3 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "24 minutes", "4 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "10 minutes", "5 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "15 minutes", "6 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "12 minutes", "7 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "12 minutes", "8 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "15 minutes", "9 hours ago"))
        callHistoryList.add(ModelCallHistory("John Doe", "12 minutes", "10 hours ago"))

        mAdapterCallHistory?.setItems(callHistoryList)
    }

    override fun onClick(view: View?) {

    }

}