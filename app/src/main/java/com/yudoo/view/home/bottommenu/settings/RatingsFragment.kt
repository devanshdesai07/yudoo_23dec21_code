package com.yudoo.view.home.bottommenu.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yudoo.R
import com.yudoo.adapter.RatingAdapter
import com.yudoo.adapter.ReviewAdapter
import com.yudoo.databinding.FragmentRatingsBinding
import com.yudoo.model.ModelRating
import com.yudoo.model.ModelReview
import com.yudoo.utils.GridSpacingItemDecoration


class RatingsFragment : Fragment() {

    lateinit var binding: FragmentRatingsBinding
    private var mAdapterRating: RatingAdapter? = null
    private var mAdapterReview: ReviewAdapter? = null

    companion object {

        @JvmStatic
        fun newInstance() = RatingsFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentRatingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        mAdapterRating = RatingAdapter()
        binding.rvRating.layoutManager = LinearLayoutManager(requireContext())
        binding.rvRating.addItemDecoration(GridSpacingItemDecoration(1, R.dimen.size_8dp, true, 0))
        binding.rvRating.adapter = mAdapterRating


        mAdapterReview = ReviewAdapter()
        binding.rvReview.layoutManager = LinearLayoutManager(requireContext())
        binding.rvReview.addItemDecoration(GridSpacingItemDecoration(1, R.dimen.size_8dp, true, 0))
        binding.rvReview.adapter = mAdapterReview

        setData()
    }

    private fun setData() {
        val ratingList = ArrayList<ModelRating>()
        ratingList.add(ModelRating("Shop Rating", R.drawable.ic_shop, "4.5", "4.1"))
        ratingList.add(ModelRating("Product experience", R.drawable.ic_product, "4.2", "4.0"))
        ratingList.add(
            ModelRating(
                "Service experience",
                R.drawable.ic_settings,
                "4.6",
                "4.5"
            )
        )
        ratingList.add(
            ModelRating(
                "Order experience",
                R.drawable.ic_order_experience,
                "4.0",
                "3.8"
            )
        )
        mAdapterRating?.setItems(ratingList)

        val reviewList = ArrayList<ModelReview>()
        reviewList.add(ModelReview("Test review message description 1", "5 Mar 2021", "John Doe"))
        reviewList.add(ModelReview("Test review message description 2", "6 Mar 2021", "Jack"))
        reviewList.add(ModelReview("Test review message description 3", "7 Mar 2021", "Johny"))
        reviewList.add(ModelReview("Test review message description 4", "8 Mar 2021", "Jill"))
        mAdapterReview?.setItems(reviewList)
    }

}