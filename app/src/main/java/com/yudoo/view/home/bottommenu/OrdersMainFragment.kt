package com.yudoo.view.home.bottommenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yudoo.R
import com.yudoo.adapter.OrderAdapter
import com.yudoo.databinding.FragmentOrdersBinding
import com.yudoo.model.ModelOrder
import com.yudoo.utils.GridSpacingItemDecoration

class OrdersMainFragment : Fragment() {

    lateinit var binding: FragmentOrdersBinding
    private var mAdapterOrder: OrderAdapter? = null

    companion object {
        @JvmStatic
        fun newInstance() = OrdersMainFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentOrdersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        mAdapterOrder = OrderAdapter()
        binding.rvOrders.layoutManager = LinearLayoutManager(requireContext())
        binding.rvOrders.addItemDecoration(
            GridSpacingItemDecoration(
                1,
                R.dimen.size_8dp,
                true,
                0
            )
        )
        binding.rvOrders.adapter = mAdapterOrder

        setData()
    }

    private fun setData() {
        val orderList = ArrayList<ModelOrder>()
        orderList.add(
            ModelOrder(
                "Red Kurta (+3)",
                "ID: 245741",
                "PENDING"
            )
        )
        orderList.add(
            ModelOrder(
                "Red Kurta (+3)",
                "ID: 245740",
                "PENDING"
            )
        )
        orderList.add(
            ModelOrder(
                "Red Kurta (+2)",
                "ID: 245732",
                "COMPLETED"
            )
        )
        orderList.add(
            ModelOrder(
                "Red Kurta (+3)",
                "ID: 245740",
                "PENDING"
            )
        )
        orderList.add(
            ModelOrder(
                "Red Kurta (+3)",
                "ID: 245740",
                "PENDING"
            )
        )
        orderList.add(
            ModelOrder(
                "Red Kurta (+2)",
                "ID: 245732",
                "COMPLETED"
            )
        )

        mAdapterOrder?.setItems(orderList)
    }
}