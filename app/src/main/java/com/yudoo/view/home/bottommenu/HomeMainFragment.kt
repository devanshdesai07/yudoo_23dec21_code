package com.yudoo.view.home.bottommenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.R
import com.yudoo.adapter.HomeCardAdapter
import com.yudoo.databinding.FragmentHomeMainBinding
import com.yudoo.model.ModelHomeCard
import com.yudoo.utils.GridSpacingItemDecoration
import com.yudoo.utils.PreferenceHelper

class HomeMainFragment : Fragment(), OnItemClickListener<ModelHomeCard> {

    lateinit var binding: FragmentHomeMainBinding

    var ongoingAdapter: HomeCardAdapter? = null
    var customerAdapter: HomeCardAdapter? = null
    var paymentAdapter: HomeCardAdapter? = null
    var revenueAdapter: HomeCardAdapter? = null

    companion object {
        @JvmStatic
        fun newInstance() = HomeMainFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentHomeMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        ongoingAdapter = HomeCardAdapter(listener = this)
        binding.rvOrderInProgress.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.rvOrderInProgress.addItemDecoration(
            GridSpacingItemDecoration(
                2,
                R.dimen.size_8dp,
                true,
                0
            )
        )
        binding.rvOrderInProgress.adapter = ongoingAdapter

        customerAdapter = HomeCardAdapter(listener = this)
        binding.rvCustomers.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.rvCustomers.addItemDecoration(
            GridSpacingItemDecoration(
                2,
                R.dimen.size_8dp,
                true,
                0
            )
        )
        binding.rvCustomers.adapter = customerAdapter

        paymentAdapter = HomeCardAdapter(listener = this)
        binding.rvPayments.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.rvPayments.addItemDecoration(
            GridSpacingItemDecoration(
                2,
                R.dimen.size_8dp,
                true,
                0
            )
        )
        binding.rvPayments.adapter = paymentAdapter

        revenueAdapter = HomeCardAdapter(listener = this)
        binding.rvRevenue.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.rvRevenue.addItemDecoration(GridSpacingItemDecoration(2, R.dimen.size_8dp, true, 0))
        binding.rvRevenue.adapter = revenueAdapter

        binding.edtShopId.text = PreferenceHelper.getInstance().registrationData?.Id?.toString()

        val ongoingDataList = ArrayList<ModelHomeCard>()
        ongoingDataList.add(
            ModelHomeCard(
                "0",
                "Pending Online payment customers",
                icon = R.drawable.ic_pending_payment,
                color = "#0B346F",
                colorBg = "#440B346F"
            )
        )
        ongoingDataList.add(
            ModelHomeCard(
                "0",
                "Number of Pending dispatch",
                icon = R.drawable.ic_pending_dishpach,
                color = "#A4A4ED",
                colorBg = "#44A4A4ED"
            )
        )
        ongoingDataList.add(
            ModelHomeCard(
                "0",
                "In-transit",
                icon = R.drawable.ic_pending_payment,
                color = "#511957",
                colorBg = "#44511957"
            )
        )
        ongoingDataList.add(
            ModelHomeCard(
                "0",
                "Delivered (return period)",
                icon = R.drawable.ic_package_delivered,
                color = "#FFB000",
                colorBg = "#44FFB000"
            )
        )
        ongoingAdapter?.setItems(ongoingDataList)

        val customerDataList = ArrayList<ModelHomeCard>()
        customerDataList.add(
            ModelHomeCard(
                "0",
                "Pending Callbacks",
                icon = R.drawable.ic_pending_call,
                color = "#89E15F",
                colorBg = "#4489E15F"
            )
        )
        customerDataList.add(
            ModelHomeCard(
                "0",
                "Customer visits (24 hours)",
                icon = R.drawable.ic_customer_visits,
                color = "#6B3725",
                colorBg = "#446B3725"
            )
        )
        customerAdapter?.setItems(customerDataList)

        val paymentDataList = ArrayList<ModelHomeCard>()
        paymentDataList.add(
            ModelHomeCard(
                "₹ 0",
                "Payments to be received for order",
                icon = R.drawable.ic_credit_card,
                color = "#FD593D",
                colorBg = "#44FD593D"
            )
        )
        paymentAdapter?.setItems(paymentDataList)

        val revenueDataList = ArrayList<ModelHomeCard>()
        revenueDataList.add(
            ModelHomeCard(
                "0",
                "Revenue (Last 7 Days)",
                icon = R.drawable.ic_pending_payment,
                color = "#5074F3",
                colorBg = "#445074F3"
            )
        )
        revenueDataList.add(
            ModelHomeCard(
                "0",
                "Orders (Last 7 Days)",
                icon = R.drawable.ic_revenue_orders,
                color = "#159890",
                colorBg = "#44159890"
            )
        )
        revenueAdapter?.setItems(revenueDataList)
    }

    override fun onItemClick(view: View?, item: ModelHomeCard?, position: Int) {

    }
}