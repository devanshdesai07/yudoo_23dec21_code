package com.yudoo.view.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.navigation.NavigationBarView
import com.yudoo.R
import com.yudoo.databinding.ActivityHomeBinding
import com.yudoo.extensions.replaceFragments
import com.yudoo.extensions.setToolbar
import com.yudoo.view.home.bottommenu.ComingSoonFragment
import com.yudoo.view.home.bottommenu.HomeMainFragment

class HomeActivity : AppCompatActivity() {

    lateinit var binding: ActivityHomeBinding

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, HomeActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {
        setToolbar(binding.appBar.toolbar, R.string.app_name)

        binding.bottomNavigation.setOnItemSelectedListener(object :
            NavigationBarView.OnItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.actionHome -> {
                        replaceFragments(HomeMainFragment.newInstance(), false)
                        return true
                    }
                    R.id.actionCallbacks -> {
                        replaceFragments(ComingSoonFragment.newInstance(), false)
                        return true
                    }
                    R.id.actionOrders -> {
                        replaceFragments(ComingSoonFragment.newInstance(), false)
                        return true
                    }
                    R.id.actionSettings -> {
                        replaceFragments(ComingSoonFragment.newInstance(), false)
                        return true
                    }
                }
                return false
            }
        })

        binding.bottomNavigation.selectedItemId = R.id.actionHome
    }

}