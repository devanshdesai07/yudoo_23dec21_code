package com.yudoo.view.home.bottommenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.yudoo.R
import com.yudoo.databinding.FragmentCallbacksMainBinding
import com.yudoo.extensions.replaceFragments
import com.yudoo.view.home.bottommenu.callback.CallHistoryFragment
import com.yudoo.view.home.bottommenu.callback.CallbacksFragment

class CallbacksMainFragment : Fragment() {

    lateinit var binding: FragmentCallbacksMainBinding

    companion object {
        @JvmStatic
        fun newInstance() = CallbacksMainFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentCallbacksMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.call_backs)))
        binding.tabLayout.addTab(
            binding.tabLayout.newTab().setText(getString(R.string.call_history))
        )

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                onSelectTabNavigate(tab?.position ?: 0)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                onSelectTabNavigate(tab?.position ?: 0)
            }
        })

        // Select First Tab by default
        binding.tabLayout.selectTab(binding.tabLayout.getTabAt(0))

    }

    private fun onSelectTabNavigate(pos: Int) {
        when (pos) {
            0 -> replaceFragments(
                CallbacksFragment.newInstance(),
                false,
                container = R.id.containerCallback
            )
            1 -> replaceFragments(
                CallHistoryFragment.newInstance(),
                false,
                container = R.id.containerCallback
            )
        }
    }

}