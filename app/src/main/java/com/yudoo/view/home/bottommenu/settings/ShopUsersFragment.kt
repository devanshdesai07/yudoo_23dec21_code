package com.yudoo.view.home.bottommenu.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.yudoo.databinding.FragmentShopUsersBinding


class ShopUsersFragment : Fragment() {

    lateinit var binding: FragmentShopUsersBinding

    companion object {

        @JvmStatic
        fun newInstance() = ShopUsersFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentShopUsersBinding.inflate(inflater, container, false)
        return binding.root
    }

}