package com.yudoo.view.home.bottommenu.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.pcm.mvvmapidemo.listener.OnItemClickListener
import com.yudoo.R
import com.yudoo.adapter.TimeSelectionAdapter
import com.yudoo.databinding.FragmentShopInfoBinding
import com.yudoo.model.ModelTimeSelection


class ShopAddressFragment : Fragment(), View.OnClickListener, OnItemClickListener<ModelTimeSelection> {

    lateinit var binding: FragmentShopInfoBinding
    private var adapterTimeSelection: TimeSelectionAdapter? = null

    companion object {

        @JvmStatic
        fun newInstance() = ShopAddressFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentShopInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.tvBankDetails.setOnClickListener(this)
        binding.tvSelectCategories.setOnClickListener(this)
        binding.tvShopAddress.setOnClickListener(this)
        binding.tvSetTimes.setOnClickListener(this)
        binding.tvUploadImagesAndVideo.setOnClickListener(this)
        binding.tvForSearchPurpose.setOnClickListener(this)

        adapterTimeSelection = TimeSelectionAdapter(this)
        binding.rvTiming.layoutManager = LinearLayoutManager(requireContext())
        binding.rvTiming.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        binding.rvTiming.adapter = adapterTimeSelection

        setTimeData()

    }

    private fun setTimeData() {
        val list = ArrayList<ModelTimeSelection>()
        list.add(ModelTimeSelection("Monday", "10:00", "10:00", true))
        list.add(ModelTimeSelection("Tuesday", "10:00", "10:00"))
        list.add(ModelTimeSelection("Wednesday", "10:00", "10:00"))
        list.add(ModelTimeSelection("Thursday", "10:00", "10:00", true))
        list.add(ModelTimeSelection("Friday", "10:00", "10:00", true))
        list.add(ModelTimeSelection("Saturday", "10:00", "10:00", true))
        list.add(ModelTimeSelection("Sunday", "10:00", "10:00", true))
        adapterTimeSelection?.setItems(list)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.tvBankDetails -> {
                binding.elBankDetails.toggle(true)
            }
            R.id.tvSelectCategories -> {
                binding.elSelectCategories.toggle(true)
            }
            R.id.tvShopAddress -> {
                binding.elShopAddress.toggle(true)
            }
            R.id.tvSetTimes -> {
                binding.elSetTimes.toggle(true)
            }
            R.id.tvUploadImagesAndVideo -> {
                binding.elUploadImagesAndVideo.toggle(true)
            }
            R.id.tvForSearchPurpose -> {
                binding.elForSearchPurpose.toggle(true)
            }
        }
    }

    override fun onItemClick(view: View?, item: ModelTimeSelection?, position: Int) {

    }

}