package com.yudoo.view.home.bottommenu.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yudoo.R
import com.yudoo.adapter.FaqAdapter
import com.yudoo.adapter.VideoSupportAdapter
import com.yudoo.databinding.FragmentSupportBinding
import com.yudoo.model.ModelFaq
import com.yudoo.model.ModelVideo
import com.yudoo.utils.GridSpacingItemDecoration


class SupportsFragment : Fragment(), View.OnClickListener {

    lateinit var binding: FragmentSupportBinding

    private var mAdapterVideo: VideoSupportAdapter? = null
    private var mAdapterFaq: FaqAdapter? = null

    companion object {

        @JvmStatic
        fun newInstance() = SupportsFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSupportBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.tvVideo.setOnClickListener(this)
        binding.tvFaq.setOnClickListener(this)

        mAdapterVideo = VideoSupportAdapter()
        binding.rvVideos.layoutManager = LinearLayoutManager(requireContext())
        binding.rvVideos.addItemDecoration(GridSpacingItemDecoration(1, R.dimen.size_8dp, true, 0))
        binding.rvVideos.adapter = mAdapterVideo

        mAdapterFaq = FaqAdapter()
        binding.rvFaq.layoutManager = LinearLayoutManager(requireContext())
        binding.rvFaq.addItemDecoration(GridSpacingItemDecoration(1, R.dimen.size_8dp, true, 0))
        binding.rvFaq.adapter = mAdapterFaq

        setData()
    }

    private fun setData() {
        val videoList = ArrayList<ModelVideo>()
        videoList.add(ModelVideo("", "How to manage your gallery", "Test description 1"))
        videoList.add(ModelVideo("", "How to increase your selling", "Test description 1"))
        mAdapterVideo?.setItems(videoList)

        val faqList = ArrayList<ModelFaq>()
        faqList.add(ModelFaq("Where is my order?", "Test description 1"))
        faqList.add(ModelFaq("How can i exchange my product?", "Test description 2"))
        faqList.add(ModelFaq("How can i cancel my order?", "Test description 3"))
        mAdapterFaq?.setItems(faqList)
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.tvVideo->{
                binding.elVideo.toggle()
            }
            R.id.tvFaq->{
                binding.elFaq.toggle()
            }
        }
    }

}