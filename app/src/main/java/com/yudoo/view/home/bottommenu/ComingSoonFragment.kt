package com.yudoo.view.home.bottommenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.yudoo.databinding.FragmentComingSoonBinding

class ComingSoonFragment : Fragment() {


    private lateinit var binding: FragmentComingSoonBinding

    companion object {
        @JvmStatic
        fun newInstance() = ComingSoonFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentComingSoonBinding.inflate(inflater, container, false)
        return binding.root
    }

}