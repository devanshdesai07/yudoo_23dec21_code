package com.yudoo.view.home.bottommenu.callback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yudoo.R
import com.yudoo.adapter.CallbackAdapter
import com.yudoo.databinding.FragmentCallbackBinding
import com.yudoo.model.ModelCallback
import com.yudoo.utils.GridSpacingItemDecoration


class CallbacksFragment : Fragment(), View.OnClickListener {

    lateinit var binding: FragmentCallbackBinding
    private var mAdapterCallback: CallbackAdapter? = null

    companion object {

        @JvmStatic
        fun newInstance() = CallbacksFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentCallbackBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        mAdapterCallback = CallbackAdapter()
        binding.rvCallbacks.layoutManager = LinearLayoutManager(requireContext())
        binding.rvCallbacks.addItemDecoration(
            GridSpacingItemDecoration(
                1,
                R.dimen.size_8dp,
                true,
                0
            )
        )
        binding.rvCallbacks.adapter = mAdapterCallback

        setData()
    }

    private fun setData() {
        val callbackList = ArrayList<ModelCallback>()
        callbackList.add(
            ModelCallback(
                "John Doe",
                "",
                "ONLINE",
                "Total 3 past orders worth ₹ 5,000",
                "27/05/2021",
                "27/05/2021"
            )
        )
        callbackList.add(
            ModelCallback(
                "John Doe",
                "",
                "OFFLINE",
                "Total 2 past orders worth ₹ 4,000",
                "25/05/2021",
                "25/05/2021"
            )
        )
        callbackList.add(
            ModelCallback(
                "John Doe",
                "",
                "OFFLINE",
                "Total 2 past orders worth ₹ 4,000",
                "25/05/2021",
                "25/05/2021"
            )
        )
        callbackList.add(
            ModelCallback(
                "John Doe",
                "",
                "ONLINE",
                "Total 1 past orders worth ₹ 2,000",
                "20/05/2021",
                "20/05/2021"
            )
        )
        callbackList.add(
            ModelCallback(
                "John Doe",
                "",
                "ONLINE",
                "Total 4 past orders worth ₹ 8,000",
                "10/05/2021",
                "10/05/2021"
            )
        )
        mAdapterCallback?.setItems(callbackList)
    }

    override fun onClick(view: View?) {

    }

}