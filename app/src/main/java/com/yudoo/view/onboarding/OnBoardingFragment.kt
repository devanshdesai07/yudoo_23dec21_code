package com.yudoo.view.onboarding

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.yudoo.databinding.FragmentOnBoardingBinding
import com.yudoo.model.ModelOnBoarding
import com.yudoo.utils.Const

class OnBoardingFragment : Fragment() {

    lateinit var binding: FragmentOnBoardingBinding
    private var mOnBoardingData: ModelOnBoarding? = null

    companion object {
        @JvmStatic
        fun newInstance(data: ModelOnBoarding) =
            OnBoardingFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(Const.EXTRA_DATA, data)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mOnBoardingData = arguments?.getParcelable(Const.EXTRA_DATA)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentOnBoardingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.tvDescription.text = mOnBoardingData?.description
        binding.ivImage.setImageResource(mOnBoardingData?.image ?: 0)
        binding.ivEclipse.setImageResource(mOnBoardingData?.eclipseImage ?: 0)
        if (binding.ivEclipse.layoutParams is FrameLayout.LayoutParams) {
            (binding.ivEclipse.layoutParams as FrameLayout.LayoutParams).gravity =
                mOnBoardingData?.eclipseImageGravity ?: 0
        }
    }
}