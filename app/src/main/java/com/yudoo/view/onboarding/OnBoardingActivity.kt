package com.yudoo.view.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.yudoo.R
import com.yudoo.databinding.ActivityOnBoardingBinding
import com.yudoo.model.ModelFragment
import com.yudoo.model.ModelOnBoarding
import com.yudoo.utils.PreferenceHelper
import com.yudoo.view.register.RegisterOrLoginActivity

class OnBoardingActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var binding: ActivityOnBoardingBinding
    private var pagerAdapter: MyPagerAdapter? = null

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, OnBoardingActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        setContentView(binding.root)


        initView()
    }

    private fun initView() {
        val fragmentList = ArrayList<ModelFragment>()
        fragmentList.add(
            ModelFragment(
                OnBoardingFragment.newInstance(
                    ModelOnBoarding(
                        image = R.drawable.ic_on_boarding_1,
                        eclipseImage = R.drawable.ic_ellipse_1,
                        eclipseImageGravity = Gravity.START,
                        description = "Customers from around the world will come to your shop!"
                    )
                )
            )
        )
        fragmentList.add(
            ModelFragment(
                OnBoardingFragment.newInstance(
                    ModelOnBoarding(
                        image = R.drawable.ic_on_boarding_2,
                        eclipseImage = R.drawable.ic_ellipse_2,
                        eclipseImageGravity = Gravity.START,
                        description = "Customers will do shopping from your shop using video call!"
                    )
                )
            )
        )
        fragmentList.add(
            ModelFragment(
                OnBoardingFragment.newInstance(
                    ModelOnBoarding(
                        image = R.drawable.ic_on_boarding_3,
                        eclipseImage = R.drawable.ic_ellipse_3,
                        eclipseImageGravity = Gravity.END,
                        description = "Payment? Our responsibility. Be it COD or Online."
                    )
                )
            )
        )
        fragmentList.add(
            ModelFragment(
                OnBoardingFragment.newInstance(
                    ModelOnBoarding(
                        image = R.drawable.ic_on_boarding_4,
                        eclipseImage = R.drawable.ic_ellipse_4,
                        eclipseImageGravity = Gravity.START,
                        description = "Order Delivery? Our responsibility. Be it another city or state!"
                    )
                )
            )
        )
        fragmentList.add(
            ModelFragment(
                OnBoardingFragment.newInstance(
                    ModelOnBoarding(
                        image = R.drawable.ic_on_boarding_5,
                        eclipseImage = R.drawable.ic_ellipse_5,
                        eclipseImageGravity = Gravity.END,
                        description = "Do everything from single app, from order management to customer management"
                    )
                )
            )
        )

        pagerAdapter = MyPagerAdapter(this, fragmentList)
        binding.viewPagerOnBoarding.adapter = pagerAdapter

        binding.btnNext.setOnClickListener(this)
        binding.btnSkip.setOnClickListener(this)
        binding.dotsIndicator.setViewPager2(binding.viewPagerOnBoarding)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnNext -> {
                val nextItem = binding.viewPagerOnBoarding.currentItem + 1
                if (nextItem < (pagerAdapter?.itemCount ?: 0)) {
                    binding.viewPagerOnBoarding.setCurrentItem(nextItem, true)
                } else {
                    RegisterOrLoginActivity.start(this)
                    PreferenceHelper.getInstance().isOnBoardingShown = true
                }
            }
            R.id.btnSkip -> {
                RegisterOrLoginActivity.start(this)
                PreferenceHelper.getInstance().isOnBoardingShown = true
            }
        }
    }
}