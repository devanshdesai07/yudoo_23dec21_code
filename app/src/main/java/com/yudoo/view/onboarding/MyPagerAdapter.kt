package com.yudoo.view.onboarding

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.yudoo.model.ModelFragment

class MyPagerAdapter(
    fa: FragmentActivity,
    private var fragmentList: ArrayList<ModelFragment>
) :
    FragmentStateAdapter(fa) {

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(fragmentList: ArrayList<ModelFragment>) {
        this.fragmentList = fragmentList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position].fragment
    }
}